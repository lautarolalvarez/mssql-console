const sql = require('mssql');
const readline = require('readline');
require('dotenv').config();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const config = {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    server: process.env.DB_HOST,
    database: process.env.DB_NAME
}
let pool;

sql.connect(config)
  .then(newPool => {
    pool = newPool;
    return waitForQueries();
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });

sql.on('error', err => {
  console.error('SQL ERROR: ', err.message);
  process.exit(1);
});

function waitInput() {
  return new Promise(resolve => {
    rl.question('> ', resolve);
  });
}

function waitForQueries() {
  return waitInput()
    .then((query) => {
      if (query === 'exit') {
        process.exit(0);
      }
      return pool.request().query(query);
    })
    .then(results => {
      console.log(results.recordset);
      return waitForQueries();
    })
    .catch(err => {
      console.log('ERROR: ', err.message);
      return waitForQueries();
    });
}

# Mssql Console

## Configure
Modify **.env** file.

## Dependences
Run `npm install`.

## Start
Run `npm start`.

## Stop
Enter **exit** in console.
